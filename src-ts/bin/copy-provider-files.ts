import * as fs from 'fs';
import * as os from 'os';
import * as path from 'path';

import yargs from 'yargs';

const argv = yargs.options({
  to: {
    type: 'string',
    require: true,
    description: 'the folder to copy the relevant provider files to',
  },
  template: {
    type: 'string',
    require: true,
    describe: 'the structure to place files into',
  },
}).parseSync(process.argv.slice(2));

main().catch(console.error);

async function main() {
  const { to, template } = argv;
  await run('pulsechain', to, template);
  await run('pulsex', to, template);
}

async function run(project: string, to: string, template: string) {
  const files = [
    'credits.csv',
    'point-event.csv',
    'raw-reduced.csv',
  ];
  const rootDir = path.join(__dirname, '..', '..');
  const dataDir = path.join(rootDir, 'data');
  const fileStreams = await Promise.all(files.map((file) => (
    fs.promises.readFile(path.join(dataDir, project, file))
  )));

  const templateResolved = template.replace('{project}', project);
  const toDir = to.replace('~', os.homedir());
  const finalDir = path.join(toDir, templateResolved);
  try {
    await fs.promises.access(finalDir, fs.constants.R_OK | fs.constants.W_OK);
    console.log('unable to write to dir. already exists', finalDir);
    return;
  } catch (err: unknown) {
    console.log('moving forward, writing', project);
  }
  await fs.promises.mkdir(finalDir, {
    recursive: true,
  });
  await Promise.all(fileStreams.map(async (file, index) => {
    const fileName = files[index];
    const outputPath = path.join(finalDir, fileName);
    const filePath = path.join(outputPath);
    console.log(filePath);
    await fs.promises.writeFile(filePath, file);
  }));
}
