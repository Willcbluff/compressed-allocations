import * as fs from 'fs';

import BigNumber from 'bignumber.js';
import Papa from 'papaparse';

import { Address, Point, Sacrifice } from './types';

export function exportCreditsCsv(creditsCsvPath: string, addrPoints: Map<Address, bigint>) {
  const addrPointsList = [...addrPoints].sort(); // By address

  let text = '';
  for (const [addr, points] of addrPointsList) {
    const pointsHex = '0x' + points.toString(16);
    const pointsDec = points.toString();

    const row = [addr, pointsHex, pointsDec];

    const line = row.join(',') + '\n';
    text += line;
  }

  fs.writeFileSync(creditsCsvPath, text);
}

export function exportDistributionList(creditsCsvPath: string, addrPoints: Map<Address, bigint>) {
  const addrPointsList = [...addrPoints].sort(); // By address

  let text = [
    ['address', 'amount'],
  ];
  for (const [addr, points] of addrPointsList) {
    const pointsDec = points.toString();
    text.push([addr, pointsDec]);
  }

  fs.writeFileSync(creditsCsvPath, text.map((row) => row.join(',')).join('\n'));
}

export function exportRawReducedCsv(creditsCsvPath: string, txPoints: Sacrifice[]) {
  const reducedPoints = txPoints.map((tx) => ({
    mined_timestamp: tx.minedTimestamp.toISOString(),
    transaction_hash: tx.transactionHash,
    network: tx.network,
    block_id: tx.blockId,
    currency: tx.currency,
    ticker: tx.ticker,
    decimals: tx.decimals,
    source: tx.source,
    credit_address: tx.creditAddress,
    amount: tx.amount,
    // need as a float for it to make sense to people
    usd_price: new BigNumber(tx.usdPrice.toString()).dividedBy(1e18).toString(),
    advertised_for: tx.advertisedFor ? 't' : '',
    ignore: tx.ignore ? 't' : '',
    is_sens: tx.isSens ? 't' : '',
  }));
  const csv: string = Papa.unparse(reducedPoints, {
    header: true,
  });
  fs.writeFileSync(creditsCsvPath, csv);
}

type BonusAuditRow = (Point & { address: string });

export const exportBonusAudit = (filepath: string, entries: Map<string, Point[]>) => {
  const entriesByAddress = [...entries.entries()];
  const values = entriesByAddress.reduce((memo, target) => {
    const points = target[1].map((item) => ({
      address: target[0],
      ...item,
    }));
    points.forEach((point) => {
      memo.push(point);
    });
    return memo;
  }, [] as BonusAuditRow[]);
  const csv: string = Papa.unparse(values, {
    header: true,
  });
  fs.writeFileSync(filepath, csv);
};
