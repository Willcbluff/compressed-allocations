import { Network } from './types';

export type Rule = readonly [RegExp, string];

export type NetworkRuleMap<N extends Network = Network> = Record<N, Rule>;

export function ruleForNetworks<N extends Network>(rule: Rule, networks: N[]) {
  return Object.fromEntries(
    networks.map((network) => [network, rule]),
  ) as NetworkRuleMap<N>;
}

export const HASH_160_0X_LC: Rule = [
  /^0x[0-9a-f]{40}$/,
  '40 char hex address in lowercase with 0x prefix',
];

export const HASH_256_0X_LC: Rule = [
  /^0x[0-9a-f]{64}$/,
  '64 char hex address in lowercase with 0x prefix',
];

export const HASH_256_LC: Rule = [
  /^[0-9a-f]{64}$/,
  '64 char hex address in lowercase without 0x prefix',
];

export const HASH_256_UC: Rule = [
  /^[0-9A-F]{64}$/,
  '64 char hex address in uppercase without 0x prefix',
];

export const BOOLEAN: Rule = [
  /^true$|^false$/,
  'boolean (true or false)',
];

export const NONZERO_INTEGER: Rule = [
  /^[1-9][0-9]*$/,
  'non-zero integer',
];

export const POSITIVE_INTEGER: Rule = [
  /^[0-9]*$/,
  'positive integer',
];

export const NONZERO_DECIMAL: Rule = [
  /^[0-9]*$|^[1-9][0-9]*\.[0-9]*[1-9]$|^0\.[0-9]*[0-9]$/,
  'non-zero decimal',
];

export const DECIMAL: Rule = [
  /^\d*\.?\d*$/,
  'decimal',
];

export const ISO_DATETIME: Rule = [
  /^202[1-9]-[0-1][0-9]-[0-3][0-9]T[0-2][0-9]:[0-5][0-9]:[0-5][0-9]\.[0-9]{3}Z$/,
  'ISO date-time string (yyyy-mm-ddThh:mm:ss.mmmZ)',
];
export const TXHASH_SENS: Rule = [
  /^202[1-9]-[0-1][0-9]-[0-3][0-9]T[0-2][0-9]:[0-5][0-9]:[0-5][0-9]\.[0-9]{3}Z#[0-9]+$/,
  'ISO date-time string # index (yyyy-mm-ddThh:mm:ss.mmmZ#n)',
];

export const CURRENCY: Rule = [
  /^[a-z]+(-[0-9a-zA-Z_]+)*$/,
  'name or name-detail (name is lowercase alpha, detail also allows digits and _)',
];

export const STRING: Rule = [
  /.*/,
  'a string that contains one or more characters',
];
