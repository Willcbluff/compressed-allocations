import * as Papa from 'papaparse'
import * as fs from 'fs'
import * as path from 'path'
import * as paths from './paths'
import { ethers } from 'ethers'
import _ from 'lodash'

const pathToPulsechain = paths.fromProject('pulsechain')
const pathToEth2Stakers = paths.fromProject('eth2-stakers')
const pathToDataRoot = paths.fromProject('.')

main().catch(console.error)

type CsvRow = [string, string, string]

async function main() {
  const pulsechainFile = await fs.promises.readFile(pathToPulsechain.CREDITS_CSV_PATH)
  const {
    data: pulsechainCredits,
  } = Papa.parse<CsvRow>(pulsechainFile.toString(), {
    header: false,
  })
  const eth2StakersFile = await fs.promises.readFile(pathToEth2Stakers.CREDITS_CSV_PATH)
  const {
    data: eth2StakersCredits,
  } = Papa.parse<CsvRow>(eth2StakersFile.toString(), {
    header: false,
  })
  const addedCredits = _(eth2StakersCredits)
    .concat(pulsechainCredits)
    .filter(([address]) => !!address)
    .reduce((totals, [address, hexAmount, amount]) => {
      const account = ethers.getAddress(address)
      const rowAmount = ethers.toBigInt(amount)
      const existingAmount = totals.get(account) || 0n
      totals.set(account, rowAmount + existingAmount)
      return totals
    }, new Map<string, bigint>())
  const keys = [...addedCredits.keys()].sort()
  const ordered = keys.map((key) => ([
    key,
    `0x${(addedCredits.get(key) as bigint).toString(16)}`,
    addedCredits.get(key),
  ]))
  const csv = Papa.unparse(ordered, {
    header: false,
  })
  await fs.promises.writeFile(pathToDataRoot.CREDITS_CSV_PATH, csv)
}
