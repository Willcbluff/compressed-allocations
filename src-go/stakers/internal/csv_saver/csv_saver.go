package csv_saver

import (
	"compressed-allocations/src-go/utils"
	"encoding/csv"
	"os"

	"compressed-allocations/src-go/stakers/internal/types"

	"github.com/pkg/errors"
)

func SaveToCSV(depositors []types.Depositor, dest string) error {
	//nolint: gosec
	csvFile, err := os.Create(dest)
	if err != nil {
		return errors.Wrap(err, "failed to created csv file")
	}
	defer func() {
		err := csvFile.Close()
		utils.PanicFail(err)
	}()

	csvWriter := csv.NewWriter(csvFile)
	defer csvWriter.Flush()

	for _, oneDepositor := range depositors {
		err = csvWriter.Write(oneDepositor.ToCsvLine())
		if err != nil {
			return errors.Wrap(err, "failed to write single row into csv file")
		}
	}

	return nil
}

func LoadFromCSV(path string) ([]types.Depositor, error) {
	var depositors []types.Depositor
	var err error

	//nolint: gosec
	csvFile, err := os.Open(path)
	if err != nil {
		return depositors, errors.Wrap(err, "failed to open csv file")
	}
	defer func() {
		err := csvFile.Close()
		utils.PanicFail(err)
	}()

	csvReader := csv.NewReader(csvFile)
	csvLines, err := csvReader.ReadAll()
	if err != nil {
		return depositors, errors.Wrap(err, "failed to read csv lines")
	}

	depositors = make([]types.Depositor, len(csvLines))

	for i, line := range csvLines {
		depositor := types.DepositorFromCsvLine(line)
		depositors[i] = depositor
	}

	return depositors, err
}
