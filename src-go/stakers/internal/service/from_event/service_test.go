package from_event

import (
	"math/rand"
	"testing"

	"compressed-allocations/src-go/stakers/internal/config"
	"compressed-allocations/src-go/stakers/internal/types"

	"github.com/ethereum/go-ethereum/common"
	"github.com/stretchr/testify/assert"
)

func TestGetAllStakers(t *testing.T) {
	var tests = []struct {
		testName string
		cfg      *config.Config
	}{
		{
			testName: "",
			cfg: &config.Config{
				EventsBatchSize:      10,
				BatchWorkerPoolSize:  1,
				TxWorkerPoolSize:     1,
				BeginningBlockNumber: 1,
				EndingBlockNumber:    35,
			},
		},
		{
			testName: "endingBlockNumber is less than EventsBatchSize",
			cfg: &config.Config{
				EventsBatchSize:      10,
				BatchWorkerPoolSize:  1,
				TxWorkerPoolSize:     1,
				BeginningBlockNumber: 1,
				EndingBlockNumber:    5,
			},
		},
		{
			testName: "endingBlockNumber is equal to EventsBatchSize",
			cfg: &config.Config{
				EventsBatchSize:      10,
				BatchWorkerPoolSize:  1,
				TxWorkerPoolSize:     1,
				BeginningBlockNumber: 1,
				EndingBlockNumber:    10,
			},
		},
		{
			testName: "BatchWorkerPoolSize is 5",
			cfg: &config.Config{
				EventsBatchSize:      10,
				BatchWorkerPoolSize:  5,
				TxWorkerPoolSize:     1,
				BeginningBlockNumber: 1,
				EndingBlockNumber:    3552,
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.testName, func(t *testing.T) {
			svc := service{
				cfg: tt.cfg,
				eventsRetriever: &eventsRetrieverMock{
					stakers: &types.DepositorMap{},
				},
			}

			res := svc.GetAllStakers(tt.cfg.BeginningBlockNumber, tt.cfg.EndingBlockNumber)

			assert.Equal(t, int(tt.cfg.EndingBlockNumber-tt.cfg.BeginningBlockNumber+1), len(res.GetAsArray()))
		})
	}

}

type eventsRetrieverMock struct {
	totalBlockProcessed uint64
	stakers             *types.DepositorMap
}

// BatchQueryEvents adds one staker for each block, so we can ensure that we processed all blocks we needed
func (e *eventsRetrieverMock) BatchQueryEvents(start uint64, end uint64, txWorkerPoolSize int) {
	var token []byte
	for i := start; i <= end; i++ {
		token = make([]byte, 20)
		rand.Read(token)
		e.stakers.Add(common.BytesToAddress(token), 10)
	}
}

func (e *eventsRetrieverMock) GetStakers() *types.DepositorMap {
	return e.stakers
}
