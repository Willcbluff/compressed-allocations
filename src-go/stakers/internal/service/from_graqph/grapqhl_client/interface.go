package grapqhl_client

import "compressed-allocations/src-go/stakers/internal/types"

type StakingDataRetriever interface {
	GetTotalDepositorsAmount() (int, error)
	GetStakingDataByBunch(limit int, after string) ([]types.Depositor, error)
}
