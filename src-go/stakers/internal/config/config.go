package config

import (
	"fmt"

	"github.com/spf13/viper"
)

type Config struct {
	ArchiveNode  string `mapstructure:"ARCHIVE_NODE"`
	GraphAddress string `mapstructure:"GRAPH_ADDRESS"`

	EventsBatchSize     uint64 `mapstructure:"EVENTS_BATCH_SIZE"`
	GraphPageSize       int    `mapstructure:"GRAPH_PAGE_SIZE"`
	BatchWorkerPoolSize int    `mapstructure:"BATCH_WORKER_POOL_SIZE"`
	TxWorkerPoolSize    int    `mapstructure:"TX_WORKER_POOL_SIZE"`

	BeginningBlockNumber uint64 `mapstructure:"BEGINNING_BLOCK_NUMBER"`
	EndingBlockNumber    uint64 `mapstructure:"ENDING_BLOCK_NUMBER"`
}

// nolint: gosec
const (
	EventCsvDestination    = "./data/eth2-stakers/stakers-event.csv"
	TheGraphCsvDestination = "./data/eth2-stakers/stakers-graph.csv"
	CreditsCsvDestination  = "./data/eth2-stakers/credits.csv"

	BeginningBlockNumber uint64 = 11052984
	// Last ETH-Mainnet block before fork (testnet V3)
	EndingBlockNumber uint64 = 16492699

	eventsBatchSize     = 1000
	graphPageSize       = 1000
	batchWorkerPoolSize = 5
	txWorkerPoolSize    = 50
)

// Load reads env variables stored in .env file in the root of the repo
func Load() (*Config, error) {
	viper.SetConfigFile(".env")
	viper.SetDefault("BATCH_WORKER_POOL_SIZE", batchWorkerPoolSize)
	viper.SetDefault("EVENTS_BATCH_SIZE", eventsBatchSize)
	viper.SetDefault("GRAPH_PAGE_SIZE", graphPageSize)
	viper.SetDefault("TX_WORKER_POOL_SIZE", txWorkerPoolSize)
	viper.SetDefault("BEGINNING_BLOCK_NUMBER", BeginningBlockNumber)
	viper.SetDefault("ENDING_BLOCK_NUMBER", EndingBlockNumber)

	err := viper.ReadInConfig()
	if err != nil {
		return nil, err
	}

	var cfg Config
	err = viper.Unmarshal(&cfg)

	return &cfg, err
}

func (c *Config) String() string {
	return fmt.Sprintf("batchWorkerPoolSize: %v, txWorkerPoolSize: %v",
		c.BatchWorkerPoolSize,
		c.TxWorkerPoolSize,
	)
}
