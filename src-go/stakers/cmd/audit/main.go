package main

import (
	"compressed-allocations/src-go/stakers/internal/config"
	"compressed-allocations/src-go/stakers/internal/csv_saver"
	"compressed-allocations/src-go/stakers/internal/service/audit"
	"compressed-allocations/src-go/utils"

	log "github.com/sirupsen/logrus"
)

func main() {
	depositors, err := audit.CompareCSV(config.EventCsvDestination, config.TheGraphCsvDestination)
	utils.PanicFail(err)

	err = csv_saver.SaveToCSV(depositors, config.CreditsCsvDestination)
	utils.PanicFail(err)

	log.Info("Audit successful, The Graph and Event Parsing delivered the same result.")
}
